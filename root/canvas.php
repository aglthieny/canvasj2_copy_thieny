<?php $pageid="canvas1";?>
<?php include('assets/include/header.php'); ?>
<canvas id="canvasID" width="165" height="145"></canvas>
<script>
    var context = document.getElementById('canvasID').getContext('2d');
    var width = 125, height = 105, padding = 20;
    context.beginPath();
    context.moveTo(padding + width/2, padding);
    context.lineTo(padding+width, height + padding);
    context.lineTo(padding, height + padding);
    context.closePath();
    context.fillStyle = "#ffc821";
    context.fill();
    context.shadowBlur = 5;
    context.shadowColor = "black";

    var gradient = context.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, '#ffc821');
    gradient.addColorStop(1, '#faf100');

    context.lineWidth = 20;
    context.lineJoin = "round";	
    context.strokeStyle = gradient;
    context.stroke();
    context.fillStyle = gradient;
    context.fill();
    
    gradient = context.createLinearGradient(0,padding,0,padding+height);
    gradient.addColorStop(0, "transparent");
    gradient.addColorStop(0.5, "transparent");
    gradient.addColorStop(0.5, "#dcaa09"); 
    gradient.addColorStop(1, "#faf100");

    context.fillStyle = gradient;
    context.fill();
    context.lineWidth = 10;
    context.lineJoin = "round";	
    context.strokeStyle = "#333";
    context.stroke();
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.font = "bold 60px 'Times New Roman', Times, serif";
    context.fillStyle = "#333";
    try{
    context.fillText("!", padding + width/2, padding + height/1.5);
    }catch(ex){}
</script>

<?php include('assets/include/footer.php'); ?>
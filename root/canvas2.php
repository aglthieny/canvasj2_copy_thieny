<?php $pageid="canvas1";?>
<?php include('assets/include/header.php'); ?>
<style>
	#drawing-canvas {
		position: absolute;
		background-color: #000000;
		top: 0px;
		right: 0px;
		z-index: 3000;
		cursor: crosshair;
	}
</style>
<canvas id="drawing-canvas" height="128" width="128"></canvas>

<?php include('assets/include/footer.php'); ?>

'use strict';
window.ThreeBSP = (function() {
	
	var ThreeBSP,
		EPSILON = 1e-5,
		COPLANAR = 0,
		FRONT = 1,
		BACK = 2,
		SPANNING = 3;
	
	ThreeBSP = function( geometry ) {
		// Convert THREE.Geometry to ThreeBSP
		var i, _length_i,
			face, vertex, faceVertexUvs, uvs,
			polygon,
			polygons = [],
			tree;
	
		if ( geometry instanceof THREE.Geometry ) {
			this.matrix = new THREE.Matrix4;
		} else if ( geometry instanceof THREE.Mesh ) {
			// #todo: add hierarchy support
			geometry.updateMatrix();
			this.matrix = geometry.matrix.clone();
			geometry = geometry.geometry;
		} else if ( geometry instanceof ThreeBSP.Node ) {
			this.tree = geometry;
			this.matrix = new THREE.Matrix4;
			return this;
		} else {
			throw 'ThreeBSP: Given geometry is unsupported';
		}
	
		for ( i = 0, _length_i = geometry.faces.length; i < _length_i; i++ ) {
			face = geometry.faces[i];
			faceVertexUvs = geometry.faceVertexUvs[0][i];
			polygon = new ThreeBSP.Polygon;
			
			if ( face instanceof THREE.Face3 ) {
				vertex = geometry.vertices[ face.a ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[0].x, faceVertexUvs[0].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[0], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
				
				vertex = geometry.vertices[ face.b ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[1].x, faceVertexUvs[1].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[1], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
				
				vertex = geometry.vertices[ face.c ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[2].x, faceVertexUvs[2].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[2], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
			} else if ( typeof THREE.Face4 ) {
				vertex = geometry.vertices[ face.a ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[0].x, faceVertexUvs[0].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[0], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
				
				vertex = geometry.vertices[ face.b ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[1].x, faceVertexUvs[1].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[1], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
				
				vertex = geometry.vertices[ face.c ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[2].x, faceVertexUvs[2].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[2], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
				
				vertex = geometry.vertices[ face.d ];
                                uvs = faceVertexUvs ? new THREE.Vector2( faceVertexUvs[3].x, faceVertexUvs[3].y ) : null;
                                vertex = new ThreeBSP.Vertex( vertex.x, vertex.y, vertex.z, face.vertexNormals[3], uvs );
				vertex.applyMatrix4(this.matrix);
				polygon.vertices.push( vertex );
			} else {
				throw 'Invalid face type at index ' + i;
			}
			
			polygon.calculateProperties();
			polygons.push( polygon );
		};
	
		this.tree = new ThreeBSP.Node( polygons );
	};
	ThreeBSP.prototype.subtract = function( other_tree ) {
		var a = this.tree.clone(),
			b = other_tree.tree.clone();
		
		a.invert();
		a.clipTo( b );
		b.clipTo( a );
		b.invert();
		b.clipTo( a );
		b.invert();
		a.build( b.allPolygons() );
		a.invert();
		a = new ThreeBSP( a );
		a.matrix = this.matrix;
		return a;
	};
	ThreeBSP.prototype.union = function( other_tree ) {
		var a = this.tree.clone(),
			b = other_tree.tree.clone();
		
		a.clipTo( b );
		b.clipTo( a );
		b.invert();
		b.clipTo( a );
		b.invert();
		a.build( b.allPolygons() );
		a = new ThreeBSP( a );
		a.matrix = this.matrix;
		return a;
	};
	ThreeBSP.prototype.intersect = function( other_tree ) {
		var a = this.tree.clone(),
			b = other_tree.tree.clone();
		
		a.invert();
		b.clipTo( a );
		b.invert();
		a.clipTo( b );
		b.clipTo( a );
		a.build( b.allPolygons() );
		a.invert();
		a = new ThreeBSP( a );
		a.matrix = this.matrix;
		return a;
	};
	ThreeBSP.prototype.toGeometry = function() {
		var i, j,
			matrix = new THREE.Matrix4().getInverse( this.matrix ),
			geometry = new THREE.Geometry(),
			polygons = this.tree.allPolygons(),
			polygon_count = polygons.length,
			polygon, polygon_vertice_count,
			vertice_dict = {},
			vertex_idx_a, vertex_idx_b, vertex_idx_c,
			vertex, face,
			verticeUvs;
	
		for ( i = 0; i < polygon_count; i++ ) {
			polygon = polygons[i];
			polygon_vertice_count = polygon.vertices.length;
			
			for ( j = 2; j < polygon_vertice_count; j++ ) {
				verticeUvs = [];
				
				vertex = polygon.vertices[0];
				verticeUvs.push( new THREE.Vector2( vertex.uv.x, vertex.uv.y ) );
				vertex = new THREE.Vector3( vertex.x, vertex.y, vertex.z );
				vertex.applyMatrix4(matrix);
				
				if ( typeof vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ] !== 'undefined' ) {
					vertex_idx_a = vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ];
				} else {
					geometry.vertices.push( vertex );
					vertex_idx_a = vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ] = geometry.vertices.length - 1;
				}
				
				vertex = polygon.vertices[j-1];
				verticeUvs.push( new THREE.Vector2( vertex.uv.x, vertex.uv.y ) );
				vertex = new THREE.Vector3( vertex.x, vertex.y, vertex.z );
				vertex.applyMatrix4(matrix);
				if ( typeof vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ] !== 'undefined' ) {
					vertex_idx_b = vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ];
				} else {
					geometry.vertices.push( vertex );
					vertex_idx_b = vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ] = geometry.vertices.length - 1;
				}
				
				vertex = polygon.vertices[j];
				verticeUvs.push( new THREE.Vector2( vertex.uv.x, vertex.uv.y ) );
				vertex = new THREE.Vector3( vertex.x, vertex.y, vertex.z );
				vertex.applyMatrix4(matrix);
				if ( typeof vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ] !== 'undefined' ) {
					vertex_idx_c = vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ];
				} else {
					geometry.vertices.push( vertex );
					vertex_idx_c = vertice_dict[ vertex.x + ',' + vertex.y + ',' + vertex.z ] = geometry.vertices.length - 1;
				}
				
				face = new THREE.Face3(
					vertex_idx_a,
					vertex_idx_b,
					vertex_idx_c,
					new THREE.Vector3( polygon.normal.x, polygon.normal.y, polygon.normal.z )
				);
				
				geometry.faces.push( face );
				geometry.faceVertexUvs[0].push( verticeUvs );
			}
			
		}
		return geometry;
	};
	ThreeBSP.prototype.toMesh = function( material ) {
		var geometry = this.toGeometry(),
			mesh = new THREE.Mesh( geometry, material );
		
		mesh.position.setFromMatrixPosition( this.matrix );
		mesh.rotation.setFromRotationMatrix( this.matrix );
		
		return mesh;
	};
	
	
	ThreeBSP.Polygon = function( vertices, normal, w ) {
		if ( !( vertices instanceof Array ) ) {
			vertices = [];
		}
		
		this.vertices = vertices;
		if ( vertices.length > 0 ) {
			this.calculateProperties();
		} else {
			this.normal = this.w = undefined;
		}
	};
	ThreeBSP.Polygon.prototype.calculateProperties = function() {
		var a = this.vertices[0],
			b = this.vertices[1],
			c = this.vertices[2];
			
		this.normal = b.clone().subtract( a ).cross(
			c.clone().subtract( a )
		).normalize();
		
		this.w = this.normal.clone().dot( a );
		
		return this;
	};
	ThreeBSP.Polygon.prototype.clone = function() {
		var i, vertice_count,
			polygon = new ThreeBSP.Polygon;
		
		for ( i = 0, vertice_count = this.vertices.length; i < vertice_count; i++ ) {
			polygon.vertices.push( this.vertices[i].clone() );
		};
		polygon.calculateProperties();
		
		return polygon;
	};
	
	ThreeBSP.Polygon.prototype.flip = function() {
		var i, vertices = [];
		
		this.normal.multiplyScalar( -1 );
		this.w *= -1;
		
		for ( i = this.vertices.length - 1; i >= 0; i-- ) {
			vertices.push( this.vertices[i] );
		};
		this.vertices = vertices;
		
		return this;
	};
	ThreeBSP.Polygon.prototype.classifyVertex = function( vertex ) {  
		var side_value = this.normal.dot( vertex ) - this.w;
		
		if ( side_value < -EPSILON ) {
			return BACK;
		} else if ( side_value > EPSILON ) {
			return FRONT;
		} else {
			return COPLANAR;
		}
	};
	ThreeBSP.Polygon.prototype.classifySide = function( polygon ) {
		var i, vertex, classification,
			num_positive = 0,
			num_negative = 0,
			vertice_count = polygon.vertices.length;
		
		for ( i = 0; i < vertice_count; i++ ) {
			vertex = polygon.vertices[i];
			classification = this.classifyVertex( vertex );
			if ( classification === FRONT ) {
				num_positive++;
			} else if ( classification === BACK ) {
				num_negative++;
			}
		}
		
        if ( num_positive === vertice_count && num_negative === 0 ) {
            return FRONT;
        } else if ( num_positive === 0 && num_negative === vertice_count ) {
            return BACK;
        } else if ( num_positive > 0 && num_negative > 0 ) {
            return SPANNING;
        } else {
            return COPLANAR;
        }
	};
	ThreeBSP.Polygon.prototype.splitPolygon = function( polygon, coplanar_front, coplanar_back, front, back ) {
		var classification = this.classifySide( polygon );
		
		if ( classification === COPLANAR ) {
			
			( this.normal.dot( polygon.normal ) > 0 ? coplanar_front : coplanar_back ).push( polygon );
			
		} else if ( classification === FRONT ) {
			
			front.push( polygon );
			
		} else if ( classification === BACK ) {
			
			back.push( polygon );
			
		} else {
			
			var vertice_count,
				i, j, ti, tj, vi, vj,
				t, v,
				f = [],
				b = [];
			
			for ( i = 0, vertice_count = polygon.vertices.length; i < vertice_count; i++ ) {
				
				j = (i + 1) % vertice_count;
				vi = polygon.vertices[i];
				vj = polygon.vertices[j];
				ti = this.classifyVertex( vi );
				tj = this.classifyVertex( vj );
				
				if ( ti != BACK ) f.push( vi );
				if ( ti != FRONT ) b.push( vi );
				if ( (ti | tj) === SPANNING ) {
					t = ( this.w - this.normal.dot( vi ) ) / this.normal.dot( vj.clone().subtract( vi ) );
					v = vi.interpolate( vj, t );
					f.push( v );
					b.push( v );
				}
			}
			
			
			if ( f.length >= 3 ) front.push( new ThreeBSP.Polygon( f ).calculateProperties() );
			if ( b.length >= 3 ) back.push( new ThreeBSP.Polygon( b ).calculateProperties() );
		}
	};
	
	ThreeBSP.Vertex = function( x, y, z, normal, uv ) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.normal = normal || new THREE.Vector3;
		this.uv = uv || new THREE.Vector2;
	};
	ThreeBSP.Vertex.prototype.clone = function() {
		return new ThreeBSP.Vertex( this.x, this.y, this.z, this.normal.clone(), this.uv.clone() );
	};
	ThreeBSP.Vertex.prototype.add = function( vertex ) {
		this.x += vertex.x;
		this.y += vertex.y;
		this.z += vertex.z;
		return this;
	};
	ThreeBSP.Vertex.prototype.subtract = function( vertex ) {
		this.x -= vertex.x;
		this.y -= vertex.y;
		this.z -= vertex.z;
		return this;
	};
	ThreeBSP.Vertex.prototype.multiplyScalar = function( scalar ) {
		this.x *= scalar;
		this.y *= scalar;
		this.z *= scalar;
		return this;
	};
	ThreeBSP.Vertex.prototype.cross = function( vertex ) {
		var x = this.x,
			y = this.y,
			z = this.z;

		this.x = y * vertex.z - z * vertex.y;
		this.y = z * vertex.x - x * vertex.z;
		this.z = x * vertex.y - y * vertex.x;
		
		return this;
	};
	ThreeBSP.Vertex.prototype.normalize = function() {
		var length = Math.sqrt( this.x * this.x + this.y * this.y + this.z * this.z );
		
		this.x /= length;
		this.y /= length;
		this.z /= length;
		
		return this;
	};
	ThreeBSP.Vertex.prototype.dot = function( vertex ) {
		return this.x * vertex.x + this.y * vertex.y + this.z * vertex.z;
	};
	ThreeBSP.Vertex.prototype.lerp = function( a, t ) {
		this.add(
			a.clone().subtract( this ).multiplyScalar( t )
		);
		
		this.normal.add(
			a.normal.clone().sub( this.normal ).multiplyScalar( t )
		);
		
		this.uv.add(
			a.uv.clone().sub( this.uv ).multiplyScalar( t )
		);
		
		return this;
	};
	ThreeBSP.Vertex.prototype.interpolate = function( other, t ) {
		return this.clone().lerp( other, t );
	};
	ThreeBSP.Vertex.prototype.applyMatrix4 = function ( m ) {

		// input: THREE.Matrix4 affine matrix

		var x = this.x, y = this.y, z = this.z;

		var e = m.elements;

		this.x = e[0] * x + e[4] * y + e[8]  * z + e[12];
		this.y = e[1] * x + e[5] * y + e[9]  * z + e[13];
		this.z = e[2] * x + e[6] * y + e[10] * z + e[14];

		return this;

	}
	
	
	ThreeBSP.Node = function( polygons ) {
		var i, polygon_count,
			front = [],
			back = [];

		this.polygons = [];
		this.front = this.back = undefined;
		
		if ( !(polygons instanceof Array) || polygons.length === 0 ) return;

		this.divider = polygons[0].clone();
		
		for ( i = 0, polygon_count = polygons.length; i < polygon_count; i++ ) {
			this.divider.splitPolygon( polygons[i], this.polygons, this.polygons, front, back );
		}   
		
		if ( front.length > 0 ) {
			this.front = new ThreeBSP.Node( front );
		}
		
		if ( back.length > 0 ) {
			this.back = new ThreeBSP.Node( back );
		}
	};
	ThreeBSP.Node.isConvex = function( polygons ) {
		var i, j;
		for ( i = 0; i < polygons.length; i++ ) {
			for ( j = 0; j < polygons.length; j++ ) {
				if ( i !== j && polygons[i].classifySide( polygons[j] ) !== BACK ) {
					return false;
				}
			}
		}
		return true;
	};
	ThreeBSP.Node.prototype.build = function( polygons ) {
		var i, polygon_count,
			front = [],
			back = [];
		
		if ( !this.divider ) {
			this.divider = polygons[0].clone();
		}

		for ( i = 0, polygon_count = polygons.length; i < polygon_count; i++ ) {
			this.divider.splitPolygon( polygons[i], this.polygons, this.polygons, front, back );
		}   
		
		if ( front.length > 0 ) {
			if ( !this.front ) this.front = new ThreeBSP.Node();
			this.front.build( front );
		}
		
		if ( back.length > 0 ) {
			if ( !this.back ) this.back = new ThreeBSP.Node();
			this.back.build( back );
		}
	};
	ThreeBSP.Node.prototype.allPolygons = function() {
		var polygons = this.polygons.slice();
		if ( this.front ) polygons = polygons.concat( this.front.allPolygons() );
		if ( this.back ) polygons = polygons.concat( this.back.allPolygons() );
		return polygons;
	};
	ThreeBSP.Node.prototype.clone = function() {
		var node = new ThreeBSP.Node();
		
		node.divider = this.divider.clone();
		node.polygons = this.polygons.map( function( polygon ) { return polygon.clone(); } );
		node.front = this.front && this.front.clone();
		node.back = this.back && this.back.clone();
		
		return node;
	};
	ThreeBSP.Node.prototype.invert = function() {
		var i, polygon_count, temp;
		
		for ( i = 0, polygon_count = this.polygons.length; i < polygon_count; i++ ) {
			this.polygons[i].flip();
		}
		
		this.divider.flip();
		if ( this.front ) this.front.invert();
		if ( this.back ) this.back.invert();
		
		temp = this.front;
		this.front = this.back;
		this.back = temp;
		
		return this;
	};
	ThreeBSP.Node.prototype.clipPolygons = function( polygons ) {
		var i, polygon_count,
			front, back;

		if ( !this.divider ) return polygons.slice();
		
		front = [], back = [];
		
		for ( i = 0, polygon_count = polygons.length; i < polygon_count; i++ ) {
			this.divider.splitPolygon( polygons[i], front, back, front, back );
		}

		if ( this.front ) front = this.front.clipPolygons( front );
		if ( this.back ) back = this.back.clipPolygons( back );
		else back = [];

		return front.concat( back );
	};
	
	ThreeBSP.Node.prototype.clipTo = function( node ) {
		this.polygons = node.clipPolygons( this.polygons );
		if ( this.front ) this.front.clipTo( node );
		if ( this.back ) this.back.clipTo( node );
	};
	
	
	return ThreeBSP;
})();
var DRAWCLASS ={
  renderer:null,
  scene:null,
  camera:null,
  WIDTH:1200,
  HEIGHT:1140,
  directionLight:null,
  ambientLight:null,
  ambientLight2:null,
  init:function(){
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true ,
      canvas: document.querySelector('#myCanvas')
    });
    this.renderer.setClearColor( 0xeeffee, 0 );
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.WIDTH/2, this.HEIGHT/2);

    this.scene = new THREE.Scene();
    this.camera = new THREE.OrthographicCamera(-600, +600, 570, -570, 1, 1000);
    this.camera.position.set(100, 200, 500);
    this.camera.lookAt( new THREE.Vector3(0.0, 0.0, 0.0))

    
    this.directionLight = new THREE.DirectionalLight(0xa67bfa,0.9);
    this.directionLight.position.set( 1, 2, 1 );
    this.scene.add( this.directionLight );

    this.ambientLight = new THREE.AmbientLight(0x999999,1.0);
    this.scene.add(this.ambientLight);
    /*
    this.ambientLight2 = new THREE.AmbientLight(0x0000ff,1);
    this.scene.add(this.ambientLight2);
    */

    /*
     // åº§æ¨™è»¸è¡¨ç¤º
    var axis =  new THREE.AxisHelper(100);
    scene.add(axis);
    */
    
    this.w1_1_func();
    this.w1_2_func();
    this.w2_func();
    this.w3_func();
    this.w4_func();
    this.w5_func();
    this.w5_1_func();
    this.w6_func();
    this.w6_1_func();
    this.w8_func();
    this.w9_func();
    this.w10_func();
    this.w10_2_func();

    DRAWCLASS.renderer.render(DRAWCLASS.scene, DRAWCLASS.camera);
    //this.render_func();
  },
  render_func:function(){
    if(DEBUG){
      //console.log("rendering");
    }
    DRAWCLASS.counter_render();

    /*
    DRAWCLASS.directionLight.intensity = DRAWCLASS.startcounter/150*0.9;
    DRAWCLASS.ambientLight.intensity = DRAWCLASS.startcounter/150;
    DRAWCLASS.ambientLight2.intensity = 1 - DRAWCLASS.startcounter/150;
    */

    if(DRAWCLASS.startcounter <= 150){
      DRAWCLASS.w1_1_start();
      DRAWCLASS.w2_start();
      DRAWCLASS.w3_start();
      DRAWCLASS.w4_start();
      DRAWCLASS.w5_start();
      DRAWCLASS.w6_start();
      DRAWCLASS.w8_start();
      DRAWCLASS.w9_start();
      DRAWCLASS.w10_start();
    }else{
      DRAWCLASS.w1_1_render();
      DRAWCLASS.w2_render();
      DRAWCLASS.w3_render();
      DRAWCLASS.w4_render();
      DRAWCLASS.w5_render();
      DRAWCLASS.w5_1_render();
      DRAWCLASS.w6_render();
      DRAWCLASS.w6_1_render();
      DRAWCLASS.w8_render();
      DRAWCLASS.w9_render();
      DRAWCLASS.w10_render();
    }
    DRAWCLASS.renderer.render(DRAWCLASS.scene, DRAWCLASS.camera);
    requestAnimationFrame(DRAWCLASS.render_func);
  },
  counter:0,
  counter1:0,
  counter2:0,
  counter3:0,
  counter4:0,
  counter5:0,
  counter6:0,
  counter7:0,
  counter8:0,
  counter9:0,
  stoper:1000,
  stoper1:1000,
  stoper2:1000,
  stoper3:1000,
  stoper4:1000,
  stoper5:1000,
  stoper6:1000,
  stoper7:1000,
  stoper8:1000,
  stoper9:1000,
  startcounter:0,
  counter_render:function(){
    if(this.startcounter > 150){
      this.counter_render_main();
    }else{
      this.startcounter += 3;
    }
  },
  counter_render_main:function(){
    if(this.stoper > 600){
      if(this.counter == 0){
        this.stoper1 = 1000;
        this.stoper2 = 1000;
        this.stoper3 = 1000;
        this.stoper4 = 1000;
        this.stoper5 = 1000;
        this.stoper6 = 1000;
        this.stoper7 = 1000;
        this.stoper8 = 1000;
        this.stoper9 = 1000;
      }
      this.counter += 1;
    }else{
      this.stoper++;
    }
    //ăƒ¡ă‚¤ăƒ³ă®ă‚¿ă‚¤ăƒ ăƒ©ă‚¤ăƒ³ă‚’åˆ»ă‚€
    if(this.counter > 159){
      this.counter = 0;
      this.stoper = 0;
    }


    if(this.stoper1 > 600){
      this.counter1 += 1;
    }else{
      this.stoper1++;
    }
    if(this.counter1 > 159){
      this.counter1 = 0;
      this.stoper1 = 0;
    }
    if(this.stoper2 > 600){
      this.counter2 += 1;
    }else{
      this.stoper2++;
    }
    if(this.counter2 > 159){
      this.counter2 = 0;
      this.stoper2 = 0;
    }
    if(this.stoper3 > 600){
      this.counter3 += 1;
    }else{
      this.stoper3++;
    }
    if(this.counter3 > 159){
      this.counter3 = 0;
      this.stoper3 = 0;
    }
    if(this.stoper4 > 600){
      this.counter4 += 1;
    }else{
      this.stoper4++;
    }
    if(this.counter4 > 159){
      this.counter4 = 0;
      this.stoper4 = 0;
    }
    if(this.stoper5 > 600){
      this.counter5 += 1;
    }else{
      this.stoper5++;
    }
    if(this.counter5 > 159){
      this.counter5 = 0;
      this.stoper5 = 0;
    }
    if(this.stoper6 > 600){
      this.counter6 += 1;
    }else{
      this.stoper6++;
    }
    if(this.counter6 > 159){
      this.counter6 = 0;
      this.stoper6 = 0;
    }
    if(this.stoper7 > 600){
      this.counter7 += 1;
    }else{
      this.stoper7++;
    }
    if(this.counter7 > 159){
      this.counter7 = 0;
      this.stoper7 = 0;
    }
    if(this.stoper8 > 600){
      this.counter8 += 1;
    }else{
      this.stoper8++;
    }
    if(this.counter8 > 159){
      this.counter8 = 0;
      this.stoper8 = 0;
    }
    if(this.stoper9 > 600){
      this.counter9 += 1;
    }else{
      this.stoper9++;
    }
    if(this.counter9 > 159){
      this.counter9 = 0;
      this.stoper9 = 0;
    }
  },
  w1_1_obj:null,
  w1_1_func:function(){
    var _H = 155;
    this.w1_1_obj = new THREE.Mesh(
      new THREE.CylinderGeometry(12,12,_H,50),
      new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w1_1_obj.position.set( -313, 455, 100 );
    this.scene.add(this.w1_1_obj);
  },
  w1_2_obj:null,
  w1_2_func:function(){
    var _H = 155;
    this.w1_2_obj = new THREE.Mesh(
         new THREE.CylinderGeometry(12,12,_H,50),
         new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w1_2_obj.position.set( -313, 455 - 155, 99 );
    this.scene.add(this.w1_2_obj);
  },
  w1_1_render:function(){
    this.w1_1_obj.scale.y = 1;
    this.w1_2_obj.scale.y = 1;

    this.w1_1_obj.rotation.x = Easing.easeOutBounce(this.counter1/160,0,1,1)*Math.PI;
    this.w1_1_obj.rotation.z = 2*Easing.easeOutBounce(this.counter1/160,0,1,1)*Math.PI;
    this.w1_2_obj.rotation.x = Easing.easeOutBounce(this.counter1/160,0,1,1)*Math.PI;
  },
  w1_1_start:function(){
    var _rate = Easing.easeInOutQuint(this.startcounter/150,0,1,1);
    if(_rate < 0.5){
      this.w1_2_obj.scale.y = _rate*2 + 0.001;
      this.w1_2_obj.position.y = 455 -155 - 155*(1 - _rate*2)/2;
      this.w1_1_obj.scale.y = 0;
      this.w1_1_obj.position.y = 4800;
    }else{
      this.w1_1_obj.scale.y = (_rate-0.5)*2 + 0.001;
      this.w1_1_obj.position.y = 455 - 155*(1 - (_rate-0.5)*2)/2;

      this.w1_2_obj.scale.y = 1;
      this.w1_2_obj.position.y = 455 - 155;
    }
  },
  w2_obj:null,
  w2_func:function(){
    this.w2_obj = [];
    for(var i = 0 ; i < 5 ; i++){
      var _box = new THREE.BoxGeometry(70, 35, 40);
      _box.translate(0,0,40*i);
      var _obj = new THREE.Mesh(
          _box,
          new THREE.MeshLambertMaterial({color: 0xa67bfa})
      );
      _obj.position.set(-70,300,100);
      _obj.rotation.y = Math.PI/7*2;
      this.scene.add(_obj);
      this.w2_obj.push(_obj);
    }
  },
  w2_render:function(){
    for(var i = 0 ; i < 5 ; i++){

      var _counter = this.counter2 + 300 + 15*i -60;
      _counter = _counter % 160;
      if(_counter < 20){
        this.w2_obj[i].position.y = 300 + 60*Easing.easeOutCubic(_counter/20,0,1,1)
      }else if(_counter < 70){
        this.w2_obj[i].position.y = 300 + 60 - 60*Easing.easeOutBounce((_counter-20)/50,0,1,1);
        

      }else{
        this.w2_obj[i].position.y = 300;
      }

      if(_counter > 40 && _counter < 120){
          this.w2_obj[i].scale.x = 1 + 0.3 - 0.3*Easing.easeOutElastic((_counter-40)/80,0,1,1);
          this.w2_obj[i].scale.y = 1 - 0.6 + 0.6*Easing.easeOutElastic((_counter-40)/80,0,1,1);
      }


      if(i == 0){
        if(_counter < 100){
          this.w2_obj[i].rotation.z =  -Easing.easeOutCubic(_counter/100,0,1,1)*Math.PI;
        }else{
          this.w2_obj[i].rotation.z = 0;
        }
      }
    }
  },
  w2_start:function(){
    for(var i = 0 ; i < 5 ; i++){
      var _rate = Easing.easeInOutQuint((this.startcounter-10*(5 - i))/60,0,1,1);
      if(_rate > 1){
        _rate = 1;
      }else if(_rate < 0){
        _rate = 0;
      }
      this.w2_obj[i].scale.y = _rate + 0.001;
    }
  },

  w3_obj:null,
  w3_func:function(){
    var size3 = 42;
    var Geo = new THREE.SphereGeometry(size3, 24, 24, Math.PI/2, Math.PI);
    const sampleGeometry = new THREE.CircleGeometry(size3-6, 24);
    const matrix = new THREE.Matrix4();
    var _SIZE = 6;
    sampleGeometry.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    sampleGeometry.applyMatrix( new THREE.Matrix4().makeTranslation( _SIZE-1,0,0 ) );
    var _N =160;
      for(var i = 0 ; i < _N; i++){
        var _geo = new THREE.PlaneGeometry(_SIZE, _SIZE);
        var _mat = new THREE.Matrix4();
        _geo.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
        _geo.applyMatrix( new THREE.Matrix4().makeTranslation( 0,(size3-_SIZE/2)*Math.sin(2*Math.PI*i/_N),(size3-_SIZE/2)*Math.cos(2*Math.PI*i/_N) ) );
        Geo.merge(_geo, _mat);
      }
      for(var i = 0 ; i < _N; i++){
        var _geo = new THREE.PlaneGeometry(_SIZE, _SIZE);
        var _mat = new THREE.Matrix4();
        _geo.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
        _geo.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2));
        _geo.applyMatrix( new THREE.Matrix4().makeRotationX(-2*Math.PI*i/_N) );
        _geo.applyMatrix( new THREE.Matrix4().makeTranslation( 0,(size3-_SIZE)*Math.sin(2*Math.PI*i/_N),(size3-_SIZE)*Math.cos(2*Math.PI*i/_N) ) );
        _geo.applyMatrix( new THREE.Matrix4().makeTranslation( _SIZE/2,0,0 ) );
        Geo.merge(_geo, _mat);
      }

    Geo.merge(sampleGeometry, matrix);
    this.w3_obj = new THREE.Mesh(
        Geo,
        new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w3_obj.position.set(216,316,100);
    this.w3_obj.rotation.y = Math.PI;
    this.w3_obj.rotation.z = 0.2;
    this.scene.add(this.w3_obj);
  },
  w3_render:function(){
    this.w3_obj.rotation.y =  - Easing.easeInOutQuint(this.counter3/160,0,1,1)*Math.PI*2;
    if(this.counter3 < 80){
      this.w3_obj.position.x = 216 + 40*Easing.easeOutBounce(this.counter3/80,0,1,1)
    }else{
      this.w3_obj.position.x = 216 + 40 - 40*Easing.easeOutCubic((this.counter3-80)/80,0,1,1)
    }
    this.w3_obj.position.y = 316;
  },
  w3_start:function(){
    this.w3_obj.rotation.y =  Math.PI/2*3;
    if(this.startcounter < 100){
      this.w3_obj.scale.x = Easing.easeOutCubic(this.startcounter/100,0,1,1) + 0.001;
      this.w3_obj.scale.y = Easing.easeOutCubic(this.startcounter/100,0,1,1) + 0.001;
      this.w3_obj.scale.z = Easing.easeOutCubic(this.startcounter/100,0,1,1) + 0.001;
      this.w3_obj.position.y = 316 - (1 - Easing.easeOutCubic(this.startcounter/100,0,1,1))*21;
    }else{
      this.w3_obj.rotation.y = Math.PI/2*3 + Math.PI/2*Easing.easeInOutQuint((this.startcounter -100)/50,0,1,1);
    }
    /*
    if(this.counter < 80){
      this.w3_obj.position.x = 216 + 40*Easing.easeOutBounce(this.counter/80,0,1,1)
    }else{
      this.w3_obj.position.x = 216 + 40 - 40*Easing.easeOutCubic((this.counter-80)/80,0,1,1)
    }
    */
  },
  w4_obj:null,
  w4_func:function(){
      var size3 = 114;
      var Geo = new THREE.SphereGeometry(size3, 48, 48, Math.PI/2, Math.PI);
      var sampleGeometry = new THREE.CircleGeometry(size3-10, 48);
      var matrix = new THREE.Matrix4();
      var _SIZE = 10;
      sampleGeometry.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
      sampleGeometry.applyMatrix( new THREE.Matrix4().makeTranslation( _SIZE,0,0 ) );
      Geo.merge(sampleGeometry, matrix);
     
      var _N = 270;
      for(var i = 0 ; i < _N; i++){
        var _geo = new THREE.PlaneGeometry(_SIZE, _SIZE);
        var _mat = new THREE.Matrix4();
        _geo.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
        _geo.applyMatrix( new THREE.Matrix4().makeTranslation( 0,(size3-_SIZE/2)*Math.sin(2*Math.PI*i/_N),(size3-_SIZE/2)*Math.cos(2*Math.PI*i/_N) ) );
        Geo.merge(_geo, _mat);
      }
      for(var i = 0 ; i < _N; i++){
        var _geo = new THREE.PlaneGeometry(_SIZE, _SIZE);
        var _mat = new THREE.Matrix4();
        _geo.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
        _geo.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2));
        _geo.applyMatrix( new THREE.Matrix4().makeRotationX(-2*Math.PI*i/_N) );
        _geo.applyMatrix( new THREE.Matrix4().makeTranslation( 0,(size3-_SIZE)*Math.sin(2*Math.PI*i/_N),(size3-_SIZE)*Math.cos(2*Math.PI*i/_N) ) );
        _geo.applyMatrix( new THREE.Matrix4().makeTranslation( _SIZE/2,0,0 ) );
        Geo.merge(_geo, _mat);
      }
      this.w4_obj = new THREE.Mesh(
          Geo,
          new THREE.MeshLambertMaterial({color: 0xa67bfa})
      );
      var startX = 446;
      this.w4_obj.position.set(startX,415,100);
      this.w4_obj.rotation.y = Math.PI;
      this.w4_obj.rotation.z = -0.1;
      this.scene.add(this.w4_obj);
  },
  w4_render:function(){
    var startX = 446;
    //this.w4_obj.rotation.z = -0.1 + Easing.easeInOutQuint(this.counter/160,0,1,1)*Math.PI*2;
    this.w4_obj.rotation.y = Math.PI + Easing.easeInOutQuint(this.counter4/160,0,1,1)*Math.PI*2;

    this.w4_obj.position.y = 415;

    if(this.counter4 < 80){
      this.w4_obj.position.x = startX - 60*Easing.easeOutBounce(this.counter4/80,0,1,1)
    }else{
      this.w4_obj.position.x = startX - 60 + 60*Easing.easeOutCubic((this.counter4-80)/80,0,1,1)
    }
  },
  w4_start:function(){
    this.w4_obj.rotation.y =  Math.PI/2*3;
    if(this.startcounter < 100){
      this.w4_obj.position.y = 415 - (1 - Easing.easeOutCubic(this.startcounter/100,0,1,1))*123;

      this.w4_obj.scale.x = Easing.easeOutCubic(this.startcounter/100,0,1,1) + 0.001;
      this.w4_obj.scale.y = Easing.easeOutCubic(this.startcounter/100,0,1,1) + 0.001;
      this.w4_obj.scale.z = Easing.easeOutCubic(this.startcounter/100,0,1,1) + 0.001;

    }else{
      this.w4_obj.rotation.y = Math.PI/2*3 - Math.PI/2*Easing.easeInOutQuint((this.startcounter -100)/50,0,1,1);
    }
  },
  w5_obj:null,
  w5_func:function(){
    var Geo = new THREE.Geometry();
    var _mat = new THREE.Matrix4();
    var _geo1 = new THREE.BoxGeometry(15, 100, 180);
    //_geo1.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 82.5,0,0 ) );
    Geo.merge(_geo1, _mat);
    var _geo2 = new THREE.BoxGeometry(15, 100, 180);
    //_geo1.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( -82.5,0,0 ) );
    Geo.merge(_geo2, _mat);
    var _geo3 = new THREE.BoxGeometry(15, 100, 180);
    _geo3.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo3.applyMatrix( new THREE.Matrix4().makeTranslation( 0,0,82.5 ) );
    Geo.merge(_geo3, _mat);
    var _geo4 = new THREE.BoxGeometry(15, 100, 180);
    _geo4.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo4.applyMatrix( new THREE.Matrix4().makeTranslation( 0,0,-82.5 ) );
    Geo.merge(_geo4, _mat);

    

    _geo1 = new THREE.BoxGeometry(15, 10, 180);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 82.5,55,0 ) );
    Geo.merge(_geo1, _mat);
    _geo2 = new THREE.BoxGeometry(15, 10, 120);
    _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( -82.5,55,-20 ) );
    Geo.merge(_geo2, _mat);
    _geo3 = new THREE.BoxGeometry(15, 10, 120);
    _geo3.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo3.applyMatrix( new THREE.Matrix4().makeTranslation( 20,55,82.5 ) );
    Geo.merge(_geo3, _mat);
    _geo4 = new THREE.BoxGeometry(15, 10, 180);
    _geo4.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo4.applyMatrix( new THREE.Matrix4().makeTranslation( 0,55,-82.5 ) );
    Geo.merge(_geo4, _mat);

    _geo1 = new THREE.BoxGeometry(15, 20, 180);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 82.5,70,0 ) );
    Geo.merge(_geo1, _mat);
    _geo2 = new THREE.BoxGeometry(15, 20, 180);
    _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( -82.5,70,0 ) );
    Geo.merge(_geo2, _mat);
    _geo3 = new THREE.BoxGeometry(15, 20, 180);
    _geo3.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo3.applyMatrix( new THREE.Matrix4().makeTranslation( 0,70,82.5 ) );
    Geo.merge(_geo3, _mat);
    _geo4 = new THREE.BoxGeometry(15, 20, 180);
    _geo4.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo4.applyMatrix( new THREE.Matrix4().makeTranslation( 0,70,-82.5 ) );
    Geo.merge(_geo4, _mat);

    _geo1 = new THREE.BoxGeometry(15, 20, 180);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 82.5,90,0 ) );
    Geo.merge(_geo1, _mat);
    _geo2 = new THREE.BoxGeometry(15, 20, 120);
    _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( -82.5,90,-20 ) );
    Geo.merge(_geo2, _mat);
    _geo3 = new THREE.BoxGeometry(15, 20, 120);
    _geo3.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo3.applyMatrix( new THREE.Matrix4().makeTranslation( 20,90,82.5 ) );
    Geo.merge(_geo3, _mat);
    _geo4 = new THREE.BoxGeometry(15, 20, 180);
    _geo4.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo4.applyMatrix( new THREE.Matrix4().makeTranslation( 0,90,-82.5 ) );
    Geo.merge(_geo4, _mat);
    
    _geo1 = new THREE.BoxGeometry(15, 40, 180);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 82.5,110,0 ) );
    Geo.merge(_geo1, _mat);
    _geo2 = new THREE.BoxGeometry(15, 40, 180);
    _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( -82.5,110,0 ) );
    Geo.merge(_geo2, _mat);
    _geo3 = new THREE.BoxGeometry(15, 40, 180);
    _geo3.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo3.applyMatrix( new THREE.Matrix4().makeTranslation( 0,110,82.5 ) );
    Geo.merge(_geo3, _mat);
    _geo4 = new THREE.BoxGeometry(15, 40, 180);
    _geo4.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI/2) );
    _geo4.applyMatrix( new THREE.Matrix4().makeTranslation( 0,110,-82.5 ) );
    Geo.merge(_geo4, _mat);

    //new THREE.BoxGeometry(180, 20, 180),
    this.w5_obj = new THREE.Mesh(
        Geo,
        new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w5_obj.position.set(-295,-80,100);
    this.w5_obj.rotation.y = Math.PI/3-0.05;
    this.scene.add(this.w5_obj);
  },
  w5_render:function(){
    
  },
  w5_1_obj:null,
  w5_1_func:function(){
    this.w5_1_obj = new THREE.Mesh(
      new THREE.BoxGeometry(160, 160, 160),
      new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w5_1_obj.position.set(-290,-40,100);
    this.scene.add(this.w5_1_obj);
    this.w5_1_obj.rotation.y = Math.PI/3-0.05;
  },
  w5_1_render:function(){
    if(this.counter5 < 60){
      this.w5_1_obj.scale.y = 1 - Easing.easeOutBounce(this.counter5/60,0,1,1)*0.9;
      this.w5_1_obj.position.y = -40 + 150*Easing.easeInOutQuint(this.counter5/60,0,1,1);
    }else if(this.counter5 < 120){
      this.w5_1_obj.scale.y = 0.1;
      this.w5_1_obj.position.y = 110 - 300*Easing.easeInOutQuint((this.counter5-60)/60,0,1,1);
      this.w5_1_obj.rotation.z = -Math.PI*Easing.easeInOutQuint((this.counter5-60)/60,0,1,1);
    }else{
      this.w5_1_obj.scale.y = Easing.easeOutBounce((this.counter5-120)/60,0,1,1)*0.9+0.1;
      this.w5_1_obj.position.y = -190 + 150*Easing.easeInOutQuint((this.counter5-120)/60,0,1,1);
      this.w5_1_obj.rotation.z = 0;
    }
  },
  w5_start:function(){

    if(this.startcounter < 76){
      this.w5_obj.position.y = 10000;
      this.w5_1_obj.position.y = -40 - 80 + 80 * Easing.easeInOutQuint(this.startcounter/75,0,1,1);
      this.w5_1_obj.scale.y = Easing.easeInOutQuint(this.startcounter/75,0,1,1) + 0.001;
    }else{
      this.w5_obj.position.y = -80 - 45 + 45 * Easing.easeInOutQuint((this.startcounter -75)/75,0,1,1);
      this.w5_obj.scale.y = Easing.easeInOutQuint((this.startcounter -75)/75,0,1,1) + 0.001;
    }
    
  },
  w6_obj:null,
  w6_func:function(){
    var size3 = 52;
    var Geo = new THREE.SphereGeometry(size3, 24, 24);
    this.w6_obj = new THREE.Mesh(
        Geo,
        new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w6_obj.position.set(117,-5,100);
    this.scene.add(this.w6_obj);
  },
  w6_render:function(){
    if(this.counter6 < 80){
      this.w6_obj.scale.x = 1 - 0.3*Easing.easeInOutQuint(this.counter6/80,0,1,1);
      this.w6_obj.scale.y = 1 - 0.3*Easing.easeInOutQuint(this.counter6/80,0,1,1);
      this.w6_obj.scale.z = 1 - 0.3*Easing.easeInOutQuint(this.counter6/80,0,1,1);
    }else{
      this.w6_obj.scale.x = 0.7 + 0.3*Easing.easeInOutQuint((this.counter6-80)/80,0,1,1);
      this.w6_obj.scale.y = 0.7 + 0.3*Easing.easeInOutQuint((this.counter6-80)/80,0,1,1);
      this.w6_obj.scale.z = 0.7 + 0.3*Easing.easeInOutQuint((this.counter6-80)/80,0,1,1);
    }
    if(this.counter6%80 < 40){
        this.w6_obj.position.y = -55 + 70*Easing.easeOutCubic(this.counter6%80/40,0,1,1)
      }else if(this.counter6%80 < 80){
        this.w6_obj.position.y = -55 + 70 - 70*Easing.easeOutBounce((this.counter6%80-40)/40,0,1,1)
      }
  },
  w6_1_obj:null,
  w6_1_func:function(){
    this.w6_1_obj = new THREE.Mesh(
        new THREE.BoxGeometry(75, 75, 75),
        new THREE.MeshLambertMaterial({color: 0xa67bfa})
    );
    this.w6_1_obj.position.set(117,-55,100);
    this.scene.add(this.w6_1_obj);
    this.w6_1_obj.rotation.y = Math.PI/3-0.05;
  },
  w6_1_render:function(){
    this.w6_1_obj.rotation.x =  (this.counter6/160)*Math.PI;
    this.w6_1_obj.rotation.y =  (this.counter6/160)*Math.PI;
    if(this.counter6 < 80){
      this.w6_1_obj.scale.x = 0.7 + 0.3*Easing.easeInOutQuint(this.counter6/80,0,1,1);
      this.w6_1_obj.scale.y = 0.7 + 0.3*Easing.easeInOutQuint(this.counter6/80,0,1,1);
      this.w6_1_obj.scale.z = 0.7 + 0.3*Easing.easeInOutQuint(this.counter6/80,0,1,1);
    }else{
      this.w6_1_obj.scale.x = 1 - 0.3*Easing.easeInOutQuint((this.counter6-80)/80,0,1,1);
      this.w6_1_obj.scale.y = 1 - 0.3*Easing.easeInOutQuint((this.counter6-80)/80,0,1,1);
      this.w6_1_obj.scale.z = 1 - 0.3*Easing.easeInOutQuint((this.counter6-80)/80,0,1,1);
    }
    if(this.counter6%80 < 40){
        this.w6_1_obj.position.y = -55 + 60*Easing.easeOutCubic(this.counter6%80/40,0,1,1)
      }else if(this.counter6%80 < 80){
        this.w6_1_obj.position.y = -55 + 60 - 60*Easing.easeOutBounce((this.counter6%80-40)/40,0,1,1)
      }

      
  },
  w6_start:function(){
    this.w6_obj.scale.x = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w6_obj.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w6_obj.scale.z = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w6_obj.position.y = -55 - 50 + 50*Easing.easeInOutQuint(this.startcounter/150,0,1,1);
    this.w6_1_obj.position.y = 30000;
  },
  w8_obj:null,
  w8_func:function(){
    var _HEIGHT = 330;
    this.w8_obj = new THREE.Mesh(
         new THREE.CylinderGeometry(12,12,_HEIGHT,50),
         new THREE.MeshLambertMaterial({
                   color: 0xa67bfa
          }));
    this.w8_obj.rotation.x = 0;
    this.w8_obj.position.set( 404, 45, 100 );
    this.scene.add(this.w8_obj);
  },
  w8_render:function(){
    this.w8_obj.rotation.x = Easing.easeInOutQuint(this.counter7/160,0,1,1)*Math.PI;
    this.w8_obj.rotation.z = 2*Easing.easeInOutQuint(this.counter7/160,0,1,1)*Math.PI;
  },
  w8_start:function(){
    this.w8_obj.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w8_obj.position.y = 45 - 165 + 165*Easing.easeInOutQuint(this.startcounter/150,0,1,1);
  },
  w9_obj:null,
  w9_obj1:null,
  w9_obj2:null,
  w9_obj3:null,
  w9_obj4:null,
  w9_func:function(){
    (function(){
      var _geo = new THREE.Geometry();
      var _mat = new THREE.Matrix4();
      var _geo1 = new THREE.CylinderGeometry(0,50,50*Math.sqrt(2),3,4);
      _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,50*Math.sqrt(2)/2,0 ) );
      _geo.merge(_geo1, _mat);
      DRAWCLASS.w9_obj1 = raction(new THREE.Mesh(_geo,new THREE.MeshLambertMaterial({color: 0xa67bfa,
                 wireframe: false})));
      DRAWCLASS.w9_obj1.position.set( -208, -445, 100 );
      DRAWCLASS.scene.add(DRAWCLASS.w9_obj1);
    })();
    
    
    (function(){//æ‰‹å‰
      var _geo = new THREE.Geometry();
      var _mat = new THREE.Matrix4();
      var _geo1 = new THREE.CylinderGeometry(0,50,50*Math.sqrt(2),3,4);
      _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,-50*Math.sqrt(2)/2,50 ) );
      _geo.merge(_geo1, _mat);
      DRAWCLASS.w9_obj2 = raction(new THREE.Mesh(_geo,new THREE.MeshLambertMaterial({color: 0xa67bfa,
                 wireframe: false})));
      DRAWCLASS.w9_obj2.position.set( -208, -445, 100 );
      DRAWCLASS.scene.add(DRAWCLASS.w9_obj2);
    })();
    
    (function(){//å·¦å¥¥
      var _geo = new THREE.Geometry();
      var _mat = new THREE.Matrix4();
      var _geo1 = new THREE.CylinderGeometry(0,50,50*Math.sqrt(2),3,4);
      _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( -50*Math.sqrt(3)/2,-50*Math.sqrt(2)/2,-50/2 ) );
      _geo.merge(_geo1, _mat);
      DRAWCLASS.w9_obj3 = raction(new THREE.Mesh(_geo,new THREE.MeshLambertMaterial({color: 0xa67bfa,
                 wireframe: false})));
      //_obj1.rotation.y = Math.PI;
      DRAWCLASS.w9_obj3.position.set( -208, -445, 100 );
      DRAWCLASS.scene.add(DRAWCLASS.w9_obj3);
    })();
    (function(){//å³å¥¥
      var _geo = new THREE.Geometry();
      var _mat = new THREE.Matrix4();
      var _geo1 = new THREE.CylinderGeometry(0,50,50*Math.sqrt(2),3,120);
      _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 50*Math.sqrt(3)/2,-50*Math.sqrt(2)/2,-50/2 ) );
      _geo.merge(_geo1, _mat);
      DRAWCLASS.w9_obj4 = distraction(new THREE.Mesh(_geo,new THREE.MeshLambertMaterial({color: 0xa67bfa,
                 wireframe: false})));
      //_obj1.rotation.y = Math.PI;
      DRAWCLASS.w9_obj4.position.set( -208, -445, 100 );
      DRAWCLASS.scene.add(DRAWCLASS.w9_obj4);
    })();

    (function(){
      var _geo = new THREE.Geometry();
      var _mat = new THREE.Matrix4();
      //ä¸‹éƒ¨ă®ä¸‰è§’å½¢ă®å¤–æ¥å††
      //é«˜ă•-50/Math.sqrt(2)*2
      var r = 150/Math.sqrt(6);
      var _geo1 = new THREE.CylinderGeometry(0,r,r,4,1);
      _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,r/2,0 ) );
      _geo.merge(_geo1, _mat);
      
      var _geo2 = new THREE.CylinderGeometry(0,r,r,4,1);
      _geo2.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI));
      _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( 0,-r/2,0 ) );
      _geo.merge(_geo2, _mat);
      
      //
      _geo.applyMatrix( new THREE.Matrix4().makeRotationY(Math.PI/4));
      _geo.applyMatrix( new THREE.Matrix4().makeRotationX(Math.atan2(Math.sqrt(2),1)));
      
      _geo.applyMatrix( new THREE.Matrix4().makeTranslation( 0,-50*Math.sqrt(2)/2,0 ) );

      var _obj = new THREE.Mesh(
         _geo,
         new THREE.MeshLambertMaterial({
                   color: 0xa67bfa
          }));
      DRAWCLASS.w9_obj = raction(_obj);
      DRAWCLASS.w9_obj.position.set( -208, -445, 100 );
      DRAWCLASS.scene.add(DRAWCLASS.w9_obj);
    })();


  },
  w9_render:function(){
    //return;
    
    //this.w9_obj.rotation.x = Easing.easeInOutQuint(this.counter/160,0,1,1)*Math.PI*2;
    this.w9_obj.rotation.y = 0.15+Easing.easeInOutQuint(this.counter8/160,0,1,1)*Math.PI*2;
    this.w9_obj1.rotation.y = 0.15-Easing.easeInOutQuint(this.counter8/160,0,1,1)*Math.PI*2;
    this.w9_obj2.rotation.y = 0.15-Easing.easeInOutQuint(this.counter8/160,0,1,1)*Math.PI*2;
    this.w9_obj3.rotation.y = 0.15-Easing.easeInOutQuint(this.counter8/160,0,1,1)*Math.PI*2;
    this.w9_obj4.rotation.y = 0.15-Easing.easeInOutQuint(this.counter8/160,0,1,1)*Math.PI*2;
    
    if(this.counter8 < 80){
      this.w9_obj1.position.y = -445 + 30*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj2.position.y = -445 - 10*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj2.position.z = 100 + 30*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj3.position.y = -445 - 10*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj3.position.z = 100 - 15*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj3.position.x = -208 - 15*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj4.position.y = -445 - 10*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj4.position.z = 100 - 15*Easing.easeOutCubic(this.counter8/80,0,1,1);
      this.w9_obj4.position.x = -208 + 15*Easing.easeOutCubic(this.counter8/80,0,1,1);
    }else{
      this.w9_obj1.position.y = -445 + 30 - 30*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj2.position.y = -445 - 10 + 10*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj2.position.z = 100 + 30 - 30*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj3.position.y = -445 - 10 + 10*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj3.position.z = 100 - 15 + 15*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj3.position.x = -208 - 15 + 15*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj4.position.y = -445 - 10 + 10*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj4.position.z = 100 - 15 + 15*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
      this.w9_obj4.position.x = -208 + 15 - 15*Easing.easeOutCubic((this.counter8-80)/80,0,1,1);
    }
  },
  w9_start:function(){
    this.w9_obj.rotation.y = 0.15;
    this.w9_obj1.rotation.y = 0.15;
    this.w9_obj2.rotation.y = 0.15;
    this.w9_obj3.rotation.y = 0.15;
    this.w9_obj4.rotation.y = 0.15;


    this.w9_obj.scale.x = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj1.scale.x = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj2.scale.x = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj3.scale.x = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj4.scale.x = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;

    this.w9_obj.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj1.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj2.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj3.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj4.scale.y = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    
    this.w9_obj.scale.z = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj1.scale.z = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj2.scale.z = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj3.scale.z = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    this.w9_obj4.scale.z = Easing.easeInOutQuint(this.startcounter/150,0,1,1) + 0.001;
    
    this.w9_obj.position.y = -445 - 80  + 80*Easing.easeInOutQuint(this.startcounter/150,0,1,1);
    this.w9_obj1.position.y = -445 - 80  + 80*Easing.easeInOutQuint(this.startcounter/150,0,1,1);
    this.w9_obj2.position.y = -445 - 80  + 80*Easing.easeInOutQuint(this.startcounter/150,0,1,1);
    this.w9_obj3.position.y = -445 - 80  + 80*Easing.easeInOutQuint(this.startcounter/150,0,1,1);
    this.w9_obj4.position.y = -445 - 80  + 80*Easing.easeInOutQuint(this.startcounter/150,0,1,1);


  },
  w10_obj_1:null,
  w10_obj_2:null,
  w10_func:function(){
    

    var _geo = new THREE.Geometry();
    var _mat = new THREE.Matrix4();

    var _geo1 = new THREE.BoxGeometry(75,45,75);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,12.5,0 ) );
    _geo.merge(_geo1, _mat);


    this.w10_obj_1 = new THREE.Mesh(
         _geo,
         new THREE.MeshLambertMaterial({
                   color: 0xa67bfa
          }));
    this.w10_obj_1.rotation.y = -0.5;
    this.w10_obj_1.position.set( 165, -450, 100 );
    this.scene.add(this.w10_obj_1);
  },
  w10_2_func:function(){
    var _geo = new THREE.Geometry();
    var _mat = new THREE.Matrix4();

    var _geo1 = new THREE.BoxGeometry(45,30,75);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( -15,-25,0 ) );
    _geo.merge(_geo1, _mat);
    
    var _geo1 = new THREE.BoxGeometry(75,30,45);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,-25,15 ) );
    _geo.merge(_geo1, _mat);
    
    var _geo1 = new THREE.BoxGeometry(75,30,5);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,-25,-17.5 ) );
    _geo.merge(_geo1, _mat);
    
    var _geo1 = new THREE.BoxGeometry(75,30,10);
    _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 0,-25,-32.5 ) );
    _geo.merge(_geo1, _mat);
    


    this.w10_obj_2 = new THREE.Mesh(
         _geo,
         new THREE.MeshLambertMaterial({
                   color: 0xa67bfa
          }));
    this.w10_obj_2.rotation.y = -0.5;
    this.w10_obj_2.position.set( 165, -450, 100 );
    this.scene.add(this.w10_obj_2);
  },
  w10_render:function(){
    if(this.counter9 < 80){
      var _scale = 1 - Easing.easeOutBounce(this.counter9/80,0,1,1);
      if(_scale > 0.4){
        this.w10_obj_1.scale.y = (_scale-0.4)/0.6 + 0.001;
        this.w10_obj_1.position.y = -450 -17.5 * (1 - (_scale-0.4)/0.6)-1;
      }else{
        this.w10_obj_1.position.y = -4350;
        this.w10_obj_2.scale.y = _scale/0.4 + 0.001;
        this.w10_obj_2.position.y = -450 - 37.5 * (1 - _scale/0.4);
        
      }
    }else{
      var _scale = Easing.easeOutBounce((this.counter9-80)/80,0,1,1);
      if(_scale > 0.4){
        this.w10_obj_1.scale.y = (_scale-0.4)/0.6 + 0.001;
        this.w10_obj_1.position.y = -450 -17.5 * (1 - (_scale-0.4)/0.6);
      }else{
        this.w10_obj_1.position.y = -4350;
        this.w10_obj_2.scale.y = _scale/0.4 + 0.001;
        this.w10_obj_2.position.y = -450 - 37.5 * (1 - _scale/0.4);
        
      }
    }
    /*
    this.w10_obj_1.rotation.x = Easing.easeInOutQuint(this.counter/160,0,1,1)*Math.PI;
    this.w10_obj_1.rotation.z = 2*Easing.easeInOutQuint(this.counter/160,0,1,1)*Math.PI;
    */
  },
	w10_start:function(){
			var _scale = Easing.easeInOutQuint(this.startcounter/150,0,1,1);
			if(_scale > 0.4){
					this.w10_obj_1.scale.y = (_scale-0.4)/0.6 + 0.001;
					this.w10_obj_1.position.y = -450 -17.5 * (1 - (_scale-0.4)/0.6)-1;
				}else{
					this.w10_obj_1.position.y = -4350;
					this.w10_obj_2.scale.y = _scale/0.4 + 0.001;
					this.w10_obj_2.position.y = -450 - 37.5 * (1 - _scale/0.4);
					
				}
	}
}
function raction(_obj){
  var origin_bsp = new ThreeBSP( _obj );
  return origin_bsp.toMesh( new THREE.MeshLambertMaterial({
                 color: 0xa67bfa
  }));
}
function distraction(_obj){

  var origin_bsp = new ThreeBSP( _obj );
  
  var diff_geometry = new THREE.Geometry();

  var _mat = new THREE.Matrix4();
  var _geo1 = new THREE.CubeGeometry(8, 36, 36,12,60,60);
  _geo1.applyMatrix( new THREE.Matrix4().makeTranslation( 50,-61,40 ) );
  _geo1.applyMatrix( new THREE.Matrix4().makeRotationY(Math.PI/3) );
  diff_geometry.merge(_geo1, _mat);

  var _geo2 = new THREE.CubeGeometry(8, 36, 36,12,60,60);
  _geo2.applyMatrix( new THREE.Matrix4().makeTranslation( 30,-61,40 ) );
  _geo2.applyMatrix( new THREE.Matrix4().makeRotationY(Math.PI/3) );
  diff_geometry.merge(_geo2, _mat);

  /*
  var _obj3 = new THREE.Mesh(

       diff_geometry,
       new THREE.MeshLambertMaterial({
                 color: 0xa67bfa
        }));
    _obj3.rotation.y = 0.15;
    _obj3.position.set( -228, -425, 100 );
    DRAWCLASS.scene.add(_obj3);
    */
  var subtract_bsp = new ThreeBSP( new THREE.Mesh( diff_geometry ) );
  
  
  var _bsp = origin_bsp.subtract(subtract_bsp);
  return _bsp.toMesh( new THREE.MeshLambertMaterial({
                 color: 0xa67bfa
  }));
}

var Easing = {
  easeOutCubic: function (t, b, c, d) {
     /*if(DRAWCLASS.startcounter < 140){
        return t;
      }*/
        //
        return c*((t=t/d-1)*t*t + 1) + b;
  },
  easeOutCirc: function (t, b, c, d) {
    /*if(DRAWCLASS.startcounter < 140){
        return t;
      }*/
    return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
  },
  easeInOutQuint: function (t, b, c, d) {
    /*if(DRAWCLASS.startcounter < 140){
        return t;
      }*/
    if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
    return c/2*((t-=2)*t*t*t*t + 2) + b;
  },
  easeOutBounce: function (t, b, c, d) {
    /*if(DRAWCLASS.startcounter < 140){
        return t;
      }*/
    if ((t/=d) < (1/2.75)) {
      return c*(7.5625*t*t) + b;
    } else if (t < (2/2.75)) {
      return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
    } else if (t < (2.5/2.75)) {
      return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
    } else {
      return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
    }
  },
  easeOutElastic: function (t, b, c, d) {
    /*if(DRAWCLASS.startcounter < 140){
        return t;
      }*/
    var s=1.70158;var p=0;var a=c;
    if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
    if (a < Math.abs(c)) { a=c; var s=p/4; }
    else var s = p/(2*Math.PI) * Math.asin (c/a);
    return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
  }
}

var DEBUG = false;

$(function(){

	if(DEBUG){
	}

	if($("#myCanvas").length == 0){
		return;
	}

	var RATE = 0.1;
	var RAPID = 0.6;
	DRAWCLASS.init();
	$("#myCanvas").css("opacity",0);

	//alert($("#myCanvas").find(".-element1").length);
	$(".intrCanvas-container").find(".-element1").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper1 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element2").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper2 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element3").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper3 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element4").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper4 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element5").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper5 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element6").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper6 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element7").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper7 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element8").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper8 = 1000;
		}
	});
	$(".intrCanvas-container").find(".-element9").on({
		'mouseenter' : function(){
			DRAWCLASS.stoper9 = 1000;
		}
	});

	var TIME_DELETE = 2000;
	var TIME1 = 0*RATE;

	function ast(_t,_target){
		setTimeout(function(){
			_target.addClass("-on");
		},_t)
	}


	for(var i = 0 ; i < $(".intrFkds img").length ; i++){
		ast(TIME_DELETE - 2000 +1000*(1 - Math.random()),$(".intrFkds img").eq(i));
	}



	for(var i = 0 ; i < $(".intrTypo-container-inner div").length ; i++){
		ast(i*120,$(".intrTypo-container-inner div").eq(i));
	}
	var a = Math.floor(3*Math.random());
	var b = Math.floor(3*Math.random());

	setTimeout(function(){
		$(".intrAbout_container").addClass("-on");
		for(var i = 0 ; i < $(".intrAbout_container p").length ; i++){
			ast(600+i*100,$(".intrAbout_container p").eq(i));
		}
		deleteLine();
		$(".intrCanvas-container,.intrTypo-container").addClass('-on');
		setTimeout(function(){
			$("#anim_2").delay(600).animate({zIndex:1},{
						duration:400,
						//easing: 'easeOutQuad',
						step:function(now){
							$("#anim_2").show();
							$("#anim_5").show();
							$("#anim_10").show();
							$(".word_2_4").hide();
							$(".word_2_5").hide();
							$(".word_5_1").hide();
							$(".word_10_5").hide();
							var counter = Math.round(now*29);
							$("#anim_2").css({"background-position":"-"+(193*counter)+"px 0px"})
							$("#anim_5").css({"background-position":"-"+(260*counter)+"px 0px"})
							$("#anim_10").css({"background-position":"-"+(120*counter)+"px 0px"})
						},
						complete:function(){
							setTimeout(function(){
								$("#anim_2").hide();
								$("#anim_5").hide();
								$("#anim_10").hide();
							},30);
							$("#myCanvas").animate({"opacity":1},10);
							DRAWCLASS.render_func();
						}
			})
		},200);
	},TIME_DELETE+500);
	function deleteLine(){
		$(".word_1_3").addClass("-del");
		$(".word_2_4").addClass("-del");
		$(".word_2_5").addClass("-del");
		$(".word_3_2").addClass("-del");
		$(".word_4_2").addClass("-del");
		$(".word_5_1").addClass("-del");
		$(".word_6_4").addClass("-del");
		$(".word_8_1").addClass("-del");
		$(".word_9_4").addClass("-del");
		$(".word_10_5").addClass("-del");
	}
	setTimeout(function(){
		$(".word_1_2").addClass("-on");
	},TIME1+0*RAPID);
	setTimeout(function(){
		$(".word_1_3").addClass("-on");
	},TIME1+200*RAPID);
	setTimeout(function(){
		$(".word_1_1").addClass("-on");
	}, TIME1 + 600 * RAPID);
	

	var TIME2 = 1500*RATE;//3000;
	setTimeout(function(){
		$(".word_2_1").addClass("-on");
	},TIME2+0*RAPID);
	setTimeout(function(){
		$(".word_2_2").addClass("-on");
	},TIME2+100*RAPID);
	setTimeout(function(){
		$(".word_2_3").addClass("-on");
	},TIME2+300*RAPID);
	setTimeout(function(){
		$(".word_2_4").addClass("-on");
	}, TIME2 + 600 * RAPID);
	
	var TIME3 = 2300*RATE;//3000;
	setTimeout(function(){
		$(".word_3_1").addClass("-on");
	},TIME3+0*RAPID);
	setTimeout(function(){
		$(".word_3_2").addClass("-on");
	},TIME3+100*RAPID);
	setTimeout(function(){
		$(".word_3_3").addClass("-on");
	},TIME3+200*RAPID);
	setTimeout(function(){
		
	},TIME_DELETE);

	var TIME4 = 2600*RATE;//3000;
	setTimeout(function(){
		$(".word_4_1").addClass("-on");
	},TIME4+0*RAPID);
	setTimeout(function(){
		$(".word_4_2").addClass("-on");
	},TIME4+200*RAPID);
	setTimeout(function(){
		
	},TIME_DELETE);

	var TIME5 = 3000*RATE;//3000;
	setTimeout(function(){
		$(".word_5_1").addClass("-on");
	},TIME5+0*RAPID);
	setTimeout(function(){
		
	},TIME_DELETE);

	var TIME6 = 3400*RATE;//3000;
	setTimeout(function(){
		$(".word_6_1").addClass("-on");
	},TIME6+0*RAPID);
	setTimeout(function(){
		$(".word_6_2").addClass("-on");
	},TIME6+0*RAPID);
	setTimeout(function(){
		$(".word_6_3").addClass("-on");
	},TIME6+500*RAPID);
	setTimeout(function(){
		$(".word_6_4").addClass("-on");
	},TIME6+600*RAPID);
	setTimeout(function(){
		$(".word_6_5").addClass("-on");
	},TIME6+900*RAPID);
	setTimeout(function(){
		$(".word_6_6").addClass("-on");
	},TIME6+1000*RAPID);
	setTimeout(function(){
		
	},TIME_DELETE);

	var TIME7 = 3800*RATE;//3000;
	setTimeout(function(){
		$(".word_7_1").addClass("-on");
	},TIME7+0*RAPID);
	setTimeout(function(){
		$(".word_7_2").addClass("-on");
	},TIME7+200*RAPID);
	setTimeout(function(){
		$(".word_7_3").addClass("-on");
	},TIME7+500*RAPID);

	var TIME8 = 4200*RATE;//3000;
	setTimeout(function(){
		$(".word_8_1").addClass("-on");
	},TIME8+0*RAPID);
	setTimeout(function(){
		$(".word_8_2").addClass("-on");
	},TIME8+400*RAPID);
	setTimeout(function(){
		
	},TIME_DELETE);

	var TIME9 = 4800*RATE;//3000;
	setTimeout(function(){
		$(".word_9_1").addClass("-on");
	},TIME9+0*RAPID);
	setTimeout(function(){
		$(".word_9_2").addClass("-on");
	},TIME9+500*RAPID);
	setTimeout(function(){
		$(".word_9_3").addClass("-on");
	},TIME9+800*RAPID);
	setTimeout(function(){
		$(".word_9_4").addClass("-on");
	},TIME9+1200*RAPID);
	setTimeout(function(){
	},TIME_DELETE);

	var TIME10 = 5400*RATE;//3000;
	setTimeout(function(){
		$(".word_10_1").addClass("-on");
	},TIME10+0*RAPID);
	setTimeout(function(){
		$(".word_10_2").addClass("-on");
	},TIME10+100*RAPID);
	setTimeout(function(){
		$(".word_10_3").addClass("-on");
	},TIME10+200*RAPID);
	setTimeout(function(){
		$(".word_10_4").addClass("-on");
	},TIME10+300*RAPID);
	setTimeout(function(){
		$(".word_10_5").addClass("-on");
	},TIME10+400*RAPID);
	setTimeout(function(){
		$(".word_10_6").addClass("-on");
	},TIME10+500*RAPID);
	setTimeout(function(){
		$(".word_10_7").addClass("-on");
	},TIME10+600*RAPID);
	setTimeout(function(){
		$(".word_10_8").addClass("-on");
	},TIME10+700*RAPID);
	setTimeout(function(){
		$(".word_10_9").addClass("-on");
	},TIME10+800*RAPID);
	setTimeout(function(){
		$(".word_10_10").addClass("-on");
	},TIME10+900*RAPID);
	setTimeout(function(){
		$(".word_10_11").addClass("-on");
	},TIME10+1000*RAPID);
	setTimeout(function(){
		$(".word_10_12").addClass("-on");
	},TIME10+1100*RAPID);
	setTimeout(function(){
		$(".word_10_13").addClass("-on");
	},TIME10+1200*RAPID);
	setTimeout(function(){
		$(".word_10_14").addClass("-on");
	},TIME10+1300*RAPID);
	setTimeout(function(){
		$(".word_10_15").addClass("-on");
	},TIME10+1400*RAPID);
	setTimeout(function(){
		$(".word_10_16").addClass("-on");
	},TIME10+1500*RAPID);
	setTimeout(function(){
		$(".word_10_17").addClass("-on");
	},TIME10+1800*RAPID);
	setTimeout(function(){
	},TIME_DELETE);
});
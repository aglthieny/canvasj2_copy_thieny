<?php $pageid="index";?>
<?php include('assets/include/header.php'); ?>

<section class="topMain">
    <div class="topMain__fv">
        <div class="intrCanvas">
            <div class="intrCanvas-container">
                <canvas id="myCanvas" width="1200" height="1040"></canvas>
                <div class="-element1"></div>
                <div class="-element2"></div>
                <div class="-element3"></div>
                <div class="-element4"></div>
                <div class="-element5"></div>
                <div class="-element6"></div>
                <div class="-element7"></div>
                <div class="-element8"></div>
                <div class="-element9"></div>
            </div>
        </div>
        <div class="intrTypo">
            <div class="intrTypo-container">
                <div class="intrTypo-container-inner">
                    <div class="word_1">
                        <svg class="word_1_1" width="274px" height="219px">
                        <path d="M215.000,13.000 C215.000,13.000 102.150,158.356 84.512,175.966 C48.834,210.342 16.314,171.739 16.314,137.058 C12.839,79.726 62.790,30.592 127.805,26.109 C197.997,21.270 249.882,65.379 249.882,122.779 C249.882,159.594 233.651,182.293 215.000,196.000 "/>
                        </svg>
                        <svg class="word_1_2" width="196" height="16" viewBox="0 0 196 16">
                            <path d="M916,250h196" transform="translate(-916 -242)"/>
                        </svg>

                        <svg class="word_1_3" width="41px" height="320px">
                            <path d="M16.863,8.190 L16.863,303.278 "/>
                        </svg>
                    </div>
                    <div class="word_2">
                        <svg class="word_2_1" width="126px" height="40px">
                            <path d="M8.000,15.999 L110.000,15.999 "/>
                        </svg>
                        <svg class="word_2_2" width="96px" height="40px">
                            <path d="M8.000,15.999 L80.000,16.000 "/>
                        </svg>
                        <svg class="word_2_3" width="96px" height="228px">
                            <path d="M72.000,8.000 C72.000,8.000 72.000,141.076 72.000,166.000 C72.000,190.924 57.679,203.999 30.000,203.999 C23.964,203.999 8.000,203.999 8.000,203.999 "/>
                        </svg>

                        <svg class="word_2_4" width="178px" height="84px">
                            <path d="M16.093,8.171 L16.093,59.485 L161.605,59.485 "/>
                        </svg>
                        <svg class="word_2_5" width="178px" height="84px">
                            <path d="M16.093,8.171 L16.093,59.485 L161.605,59.485 "/>
                        </svg>

                        <div id="anim_2"></div>
                    </div>
                    <div class="word_3">
                        <svg class="word_3_1" width="90px" height="40px">
                            <path d="M8.000,16.000 L74.000,16.000 "/>
                        </svg>
                        <svg class="word_3_2" width="82px" height="108px">
                            <path d="M8.000,16.000 C8.000,16.000 9.988,16.000 24.000,16.000 C42.778,16.000 58.000,31.222 58.000,50.000 C58.000,68.778 42.778,84.000 24.000,84.000 C19.056,84.000 19.000,84.000 19.000,84.000 "/>
                        </svg>
                        <svg class="word_3_3" width="78px" height="40px">
                            <path d="M8.000,15.999 L62.000,15.999 "/>
                        </svg>
                    </div>
                    <div class="word_4">
                        <svg class="word_4_1" width="250px" height="40px">
                            <path d="M8.000,15.999 L234.000,15.999 "/>
                        </svg>
                        <svg class="word_4_2" width="169px" height="268px">
                            <path d="M118.632,16.716 C118.632,16.716 16.045,17.636 16.045,133.111 C16.045,244.256 112.938,243.834 112.938,243.834 L152.601,243.834 "/>
                        </svg>
                    </div>
                    <div class="word_5">
                        <svg class="word_5_1" width="251px" height="248px">
                            <path d="M16.936,16.536 L226.326,16.536 L226.326,223.833 L16.936,223.833 L16.936,16.536 Z"/>
                        </svg>
                        <div id="anim_5"></div>
                    </div>
                    <div class="word_6">
                        <svg class="word_6_1" width="150px" height="100px">
                            <path d="M8.000,76.000 L92.000,76.000 C92.000,76.000 126.000,76.173 126.000,41.999 C126.000,13.905 126.000,8.000 126.000,8.000 "/>
                        </svg>
                        <svg class="word_6_2" width="150px" height="260px">
                            <path d="M134.000,16.000 L50.000,16.000 C50.000,16.000 16.000,15.827 16.000,49.999 C16.000,78.095 16.000,244.000 16.000,244.000 "/>
                        </svg>
                        <svg class="word_6_3" width="126px" height="126px">
                            <path d="M59.000,8.000 C87.167,8.000 110.000,30.833 110.000,59.000 C110.000,87.166 87.167,110.000 59.000,110.000 C30.833,110.000 8.000,87.166 8.000,59.000 C8.000,30.833 30.833,8.000 59.000,8.000 Z"/>
                        </svg>
                        <svg class="word_6_4" width="132px" height="131px">
                            <path d="M61.641,16.219 C86.761,16.219 107.125,36.534 107.125,61.594 C107.125,86.654 86.761,106.969 61.641,106.969 C36.520,106.969 16.156,86.654 16.156,61.594 C16.156,36.534 36.520,16.219 61.641,16.219 Z"/>
                        </svg>
                        <svg class="word_6_5" width="40px" height="70px">
                            <path d="M16.000,8.000 L16.000,54.000 "/>
                        </svg>
                        <svg class="word_6_6" width="40px" height="70px">
                            <path d="M16.000,8.000 L16.000,54.000 "/>
                        </svg>
                    </div>

                    <div class="word_7">
                        <svg class="word_7_1" width="40px" height="70px">
                            <path d="M16.000,8.000 L16.000,53.999 "/>
                        </svg>
                        <svg class="word_7_2" width="40px" height="70px">
                            <path d="M16.000,8.000 L16.000,53.999 "/>
                        </svg>
                        <svg class="word_7_3" width="96px" height="116px">
                            <path d="M72.000,8.000 C72.000,8.000 72.000,52.576 72.000,53.999 C72.000,78.924 57.679,92.000 30.000,92.000 C23.964,92.000 8.000,92.000 8.000,92.000 "/>
                        </svg>
                    </div>
                    <div class="word_8">
                        <svg class="word_8_1" width="41px" height="324px">
                            <path d="M16.507,8.543 L16.507,307.577 "/>
                        </svg>
                        <svg class="word_8_2" width="99px" height="77px">
                            <path d="M13.000,15.000 L78.444,54.000 "/>
                        </svg>
                    </div>
                    <div class="word_9">
                        <svg class="word_9_1" width="460px" height="239px">
                            <path d="M437.580,216.475 L249.289,28.183 C249.289,28.183 225.677,3.313 201.513,27.477 C181.646,47.344 14.328,215.060 14.328,215.060 "/>
                        </svg>
                        <svg class="word_9_2" width="156px" height="40px">
                            <path d="M8.000,16.000 L140.000,16.000 "/>
                        </svg>
                        <svg class="word_9_3" width="220px" height="40px">
                            <path d="M8.000,16.000 L204.000,16.000 "/>
                        </svg>
                        <svg class="word_9_4" width="188px" height="139px">
                            <path d="M85.536,12.911 L16.766,114.569 L163.275,114.569 L120.420,62.261 "/>
                        </svg>
                    </div>
                    <div class="word_10">
                        <svg class="word_10_1" width="104px" height="40px">
                            <path d="M8.000,16.000 L88.000,16.000 "/>
                        </svg>
                        <svg class="word_10_2" width="156px" height="40px">
                            <path d="M8.000,16.000 L140.000,16.000 "/>
                        </svg>
                        <svg class="word_10_3" width="104px" height="40px">
                            <path d="M8.000,16.000 L88.000,16.000 "/>
                        </svg>
                        <svg class="word_10_4" width="104px" height="40px">
                            <path d="M8.000,16.000 L88.000,16.000 "/>
                        </svg>
                        <svg class="word_10_5" width="126px" height="126px">
                            <path d="M16.314,16.445 L101.932,16.445 L101.932,101.364 L16.314,101.364 L16.314,16.445 Z"/>
                        </svg>
                        <svg class="word_10_6" width="40px" height="52px">
                            <path d="M16.000,8.000 L16.000,36.000 "/>
                        </svg>
                        <svg class="word_10_7" width="40px" height="52px">
                            <path d="M16.000,8.000 L16.000,36.000 "/>
                        </svg>
                        <svg class="word_10_8" width="196px" height="40px">
                            <path d="M180.000,16.000 L8.000,16.000 "/>
                        </svg>
                        <svg class="word_10_9" class="word_10_1" width="40px" height="120px">
                            <path d="M16.000,8.001 L16.000,104.000 "/>
                        </svg>
                        <svg class="word_10_10" width="92px" height="40px">
                            <path d="M8.000,16.000 L76.000,16.000 "/>
                        </svg>
                        <svg class="word_10_11" width="92px" height="40px">
                            <path d="M76.000,16.000 L8.000,16.000 "/>
                        </svg>
                        <svg class="word_10_12" width="196px" height="40px">
                            <path d="M180.000,16.001 L8.000,16.001 "/>
                        </svg>
                        <svg class="word_10_13" width="218px" height="84px">
                            <path d="M202.000,60.000 L16.000,60.000 L60.000,16.001 L60.000,44.000 "/>
                        </svg>
                        <svg class="word_10_14" width="107px" height="121px">
                            <path d="M58.000,59.000 L58.185,96.627 L15.977,71.170 L86.000,15.000 "/>
                        </svg>
                        <svg class="word_10_15" width="96px" height="172px">
                            <path d="M16.000,8.000 C16.000,8.000 16.000,85.076 16.000,110.000 C16.000,134.924 30.321,148.000 58.000,148.000 C64.035,148.000 80.000,148.000 80.000,148.000 "/>
                        </svg>
                        <svg class="word_10_16" width="70px" height="40px">
                            <path d="M54.000,16.000 L8.000,16.000 "/>
                        </svg>
                        <svg class="word_10_17" width="50px" height="50px">
                            <path d="M21.000,8.000 C28.180,8.000 34.000,13.820 34.000,21.000 C34.000,28.180 28.180,34.000 21.000,34.000 C13.820,34.000 8.000,28.180 8.000,21.000 C8.000,13.820 13.820,8.000 21.000,8.000 Z"/>
                        </svg>
                        <div id="anim_10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/105/three.min.js"></script>
<?php include('assets/include/footer.php'); ?>

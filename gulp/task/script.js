/**
 * スクリプトファイルをコンパイルします。
 */

const gulp = require('gulp');
const config = require('../config');
const setting = config.setting;
const $ = require('gulp-load-plugins')(config.loadPlugins);

const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('../../webpack.config');


gulp.task('script', () => {
	gulp.src([
			setting.js.src + '**/*.js'
		])
		.pipe($.plumber({
			errorHandler: $.notify.onError('Error: <%= error %>')
		}))
		.pipe(new webpackStream(webpackConfig, webpack))
		.pipe(gulp.dest(setting.js.dest))
		.pipe($.browserSync.reload({stream: true}));
});
